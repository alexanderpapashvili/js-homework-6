// First Task
// This function calculates arithmetical average
function calcAverage(x, y, z) {
  return Math.floor((x + y + z) / 3);
}
// This function calculates arithmetical average for a specific team
let ave1;
let ave2;
function PowerRangers(x, y, z) {
  ave1 = calcAverage(x, y, z);
}
function FairyTails(x, y, z) {
  ave2 = calcAverage(x, y, z);
}
// This function checks whoi won
function checkWinner(ave1, ave2) {
  if (ave1 > ave2 * 2) {
    return `PowerRangers won! ${ave1} > ${ave2}.`;
  } else if (ave2 > ave1 * 2) {
    return `FairyTails won! ${ave2} > ${ave1}.`;
  } else {
    return "Draw";
  }
}
// First Competition
PowerRangers(44, 23, 71);
FairyTails(65, 54, 49);
console.log(checkWinner(ave1, ave2));
// Second Competition
PowerRangers(85, 54, 41);
FairyTails(23, 34, 47);
console.log(checkWinner(ave1, ave2));

// Second Task
let customer = {
  amount: [22, 295, 176, 440, 37, 105, 10, 1100, 96, 52],
  tips: [],
  sum: [],
};
let tip;
// This function calculates tips and sum amount of spent money
for (let i = 0; i < customer.amount.length; i++) {
  function calcTip(i) {
    if (customer.amount[i] > 50 && customer.amount[i] < 300) {
      tip = customer.amount[i] * 0.15;
    } else {
      tip = customer.amount[i] * 0.2;
    }
    return Math.floor(tip);
  }
  customer.tips.push(calcTip(i));
  customer.sum.push(customer.amount[i] + customer.tips[i]);
}
// This function gets array as a parametr and calculates arithmetical average
let sum = 0;
function calcArthAve(arr) {
  for (const par of arr) {
    sum += par;
  }
  sum /= arr.length;
  return sum;
}

console.log(calcArthAve(customer.tips));
console.log(calcArthAve(customer.sum));
